/**
 * @file
 * Initializing dropdown checkbox list widget.
 */

(function ($) {
    $(document).ready(function() {
        var conf;
        $(Drupal.settings.dropdown_checkboxes).each(function() {
            $("#" + $(this).attr('dcid')).dropdownchecklist({
                icon: {},
                maxDropHeight: $(this).attr('ostring')['maxDropHeight'],
                width: $(this).attr('ostring')['width'],
                firstItemChecksAll: $(this).attr('ostring')['firstItemChecksAll'],
                emptyText: $(this).attr('ostring')['emptyText'],
            });
        });
    });
})(jQuery);
