<?php

/**
 * @file
 *   drush integration for dropdown_checkboxes.
 */

/**
 * The dropdown-check-list library URI.
 */
define('DDCL_DOWNLOAD_URI', 'https://dropdown-check-list.googlecode.com/files/dropdown-check-list.1.4.zip');

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * See `drush topic docs-commands` for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function dropdown_checkboxes_drush_command() {
  $items = array();

  // The key in the $items array is the name of the command.
  $items['ddcl-library'] = array(
    'callback' => 'drush_ddcl_library',
    'description' => dt('Download and install the DDCL library.'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap.
    'arguments' => array(
      'path' => dt('Optional. A path where to install the DDCL library. If omitted Drush will use the default location.'),
    ),
    'aliases' => array('ddcl'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'.
 *
 * @param
 *   A string with the help section (prepend with 'drush:').
 *
 * @return
 *   A string with the help text for your command.
 */
function dropdown_checkboxes_drush_help($section) {
  switch ($section) {
    case 'drush:ddcl-library':
      return dt('Download and install the DDCL library from https://code.google.com/p/dropdown-check-list/downloads/list, default location is sites/all/libraries.');
  }
}

/**
 * Command to download the dropdown-check-list library.
 */
function drush_ddcl_library() {
  $args = func_get_args();
  if (!empty($args[0])) {
    $path = $args[0];
  }
  else {
    $path = 'sites/all/libraries';
  }

  // Create the path if it does not exist.
  if (!is_dir($path)) {
    drush_op('mkdir', $path);
    drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
  }

  // Set the directory to the download location.
  $olddir = getcwd();
  chdir($path);

  // Download the zip archive.
  if ($filepath = drush_download_file(DDCL_DOWNLOAD_URI)) {

    $filename = basename($filepath);
    $dirname =  basename($filepath, '.zip');

    // Remove any existing dropdown-check-list library directory.
    if (is_dir($dirname) || is_dir('ddcl')) {
      drush_delete_dir($dirname, TRUE);
      drush_delete_dir('ddcl', TRUE);
      drush_log(dt('A existing DDCL library was deleted from @path', array('@path' => $path)), 'notice');
    }

    // Decompress the zip archive.
    drush_tarball_extract($filename, $dirname);

    // Change the directory name to "ddcl" if needed.
    if ($dirname != 'ddcl') {
      drush_move_dir($dirname, 'ddcl', TRUE);
      $dirname = 'ddcl';
    }
  }

  if (is_dir($dirname)) {
    $ddcl_library_version = ltrim($dirname, 'dropdown-check-list.');
    drush_variable_set('dropdown_checkboxes_js_library_version', $ddcl_library_version);
    drush_log(dt('DDCL library has been installed in @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to install the DDCL library to @path', array('@path' => $path)), 'error');
  }

  // Set working directory back to the previous working directory.
  chdir($olddir);
}
